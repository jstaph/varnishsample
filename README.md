This repo contains a very basic Vagrant file, and a likewise very basic puppet configuration to deploy
varnish.

The varnish class:

  * installs the Varnish and EPEL yum repos
  * deploys the varnish configuration file 
  * makes certain the varnish service is running and enabled on startup.

This directory is a vagrant environment; if you have [Vagrant](http://vagrantup.com) and [VirtualBox](https://www.virtualbox.org/) installed, you can run `vagrant up` in this directory to get a very basic VM to do sample deployments, or deploy the Puppet module itself to a CentOS test machine instead.

To install Puppet on a server, I'd recommend using [PuppetLab's](http://puppetlabs.com) [yum repository](http://yum.puppetlabs.com/), which distributes Puppet and all dependencies for RHEL (5, 6, and 7), CentOS (5, 6, and 7), and Fedora.  The whole manual installation process would look something like the following, assuming a RHEL or CentOS starting point.

As root, issue these commands:

1.  `yum install http://yum.puppetlabs.com/el/6Client/products/x86_64/puppetlabs-release-6-10.noarch.rpm -y`
1.  `yum install puppet -y`

Afterward, you'll have a 'puppet' service, which isn't running by default.  Issuing `chkconfig --del puppet` will remove its System V entries as well, if having no trace of puppet in the startup process is desirable.

Once Puppet is installed, scp the 'puppet' folder to the test server, making note of the target directory.  Then on the test server, issue the command:

`puppet apply --modulepath=</PATH/TO/UPLOADED/PUPPET/DIR>/modules -e 'include varnish'` 

and watch puppet deploy two repos, and Varnish 4.

To do the same in the vagrant VM, issue the commands after installing Puppet:

`vagrant up`

`vagrant ssh`

`sudo su`

`puppet apply --modulepath=/vagrant/puppet/modules -e 'include varnish'`

I've documented the Puppet code pretty verbosely, which I encourage you to have a look at.  It's in puppet/modules/varnish/manifests/init.pp, while the varnish configuration template can be found in puppet/modules/varnish/templates/default.erb, and the 'flat file' configuration can be found in puppet/modules/varnish/files/default.vcf.